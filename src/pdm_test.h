///////////////////////////////////////////////////////////////////////
// pdm_test.h
//  Unit Tests for functions in pdm.h. 
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef PDM_TEST_H
#define PDM_TEST_H

//
//pdm_failed_test()
// Called when a test has failed. Suggested usage is to set a debugger
// to break at this function and read the argument values fn, tst, and
// res to determine the result that failed. Examine the call stack to
// determine the exact line in the test function that failed.
// 
// fn - Function number set in main_test();
// tst - Test set number in function.
// res - Result number in test set.
//
// Enters infinite loop and does not return.
//
void pdm_failed_test(uint8_t fn, uint8_t tst, uint8_t res) {
#ifdef BUILD_UNIT_TESTS
  while(1){}
#endif
}

void pdm_conv_pcm_smpl_test(uint8_t fnidx) {
#ifdef PDM_CONV_PCM_SMPL_TEST
  uint8_t tstidx = 0;
  uint8_t residx = 0;

//---------------------------------
// pdm_conv_pcm_smpl_test()
//  Test Set
//---------------------------------
  #define pdm_conv_pcm_smpl_test_set_sz 1
  pcm_dbuf_t pcm_dbuf_test_set[pdm_conv_pcm_smpl_test_set_sz] = {
    249,1,1,1,
    1,1,1,1,
  };

//---------------------------------
// pdm_conv_pcm_smpl_test()
//  Expected Results
//---------------------------------
  #define pdm_conv_pcm_smpl_test_res_sz 8

  bit_mask_t pdm_bit_cnv_test_res[pdm_conv_pcm_smpl_test_set_sz]
                                  [pdm_conv_pcm_smpl_test_res_sz] = {2,4,8,16,32,64,128,0};

  uint8_t pdm_acc_test_res[pdm_conv_pcm_smpl_test_set_sz]
                          [pdm_conv_pcm_smpl_test_res_sz] = {249,250,251,252,253,254,255,0};

  uint8_t pdm_buf_test_res[pdm_conv_pcm_smpl_test_set_sz]
                          [pdm_conv_pcm_smpl_test_res_sz] = {0,0,0,0,0,0,0,128};

//---------------------------------
// pdm_ovr_set_test()
//  Tests
//---------------------------------
  pdm_init(ADC_CLK_DIV_128);

  for(tstidx = 0; tstidx < pdm_conv_pcm_smpl_test_set_sz; ++tstidx) {
    for(residx = 0; residx < pdm_conv_pcm_smpl_test_res_sz; ++residx) {
      pdm_conv_pcm_smpl(pcm_dbuf_test_set[tstidx][residx]);

      if(pdm_bit_cnv != pdm_bit_cnv_test_res[tstidx][residx]) {
        pdm_failed_test(fnidx,tstidx,residx);
      }

      if(pdm_acc != pdm_acc_test_res[tstidx][residx]) {
        pdm_failed_test(fnidx,tstidx,residx);
      }

//       if(pdm_buf[pdm_wr_idx] != pdm_buf_test_res[tstidx][residx]) {
//         pdm_failed_test(fnidx,tstidx,residx);
//       }
    }
  }
#endif
}

void pdm_ovr_set_test(uint8_t fnidx) {
#ifdef PDM_OVR_SET_TEST
  uint8_t tstidx = 0;
  uint8_t residx = 0;

//---------------------------------
// pdm_ovr_set_test()
//  Test Set
//---------------------------------
  #define pdm_ovr_set_test_set_sz 1
  uint8_t test_set[pdm_ovr_set_test_set_sz][5] = {0,1,2,4,8};

//---------------------------------
// pdm_ovr_set_test()
//  Expected Results
//---------------------------------
  #define pdm_ovr_set_test_res_sz 5
  uint8_t pdm_ovr_test_res[pdm_ovr_set_test_set_sz]
                          [pdm_ovr_set_test_res_sz] = {1,1,2,4,1};

  uint8_t pdm_smpls_per_byte_test_res[pdm_ovr_set_test_set_sz]
                                     [pdm_ovr_set_test_res_sz] = {8,8,4,2,8};

//---------------------------------
// pdm_ovr_set_test()
//  Tests
//---------------------------------
  pdm_init(ADC_CLK_DIV_128);

  for(tstidx = 0; tstidx < pdm_ovr_set_test_set_sz; ++tstidx) {
    for(residx = 0; residx < pdm_ovr_set_test_res_sz; ++residx) {
      pdm_ovr_set(test_set[tstidx][residx]);
      if(pdm_ovr != pdm_ovr_test_res[tstidx][residx]) {
        pdm_failed_test(fnidx,tstidx,residx);
      }

      if(pdm_smpls_per_byte != pdm_smpls_per_byte_test_res[tstidx][residx]) {
        pdm_failed_test(fnidx,tstidx,residx);
      }
    }
  }
#endif
}

void pdm_sine_out_test(uint8_t fnidx) {
#ifdef PDM_SINE_OUT_TEST
  uint8_t cur_smpl = 0;

  pdm_init(ADC_CLK_DIV_128); //9.6kHz sample rate.
  pdm_ovr_set(2);            //2x Oversampling.
  UBRR0 = 1;
  
  while(1) {
    if(!pdm_buf_amt) {
//PDM buffer isn't full. Calculate another byte.
      pdm_bit_cnv = bit(0);   //Reset the bit mask.
      pdm_buf[pdm_wr_idx] = 0; //Reset the current pdm buffer byte to all zero.

      while(pdm_bit_cnv) {
//Mask is left shifted without carry. When all bits in the current pdm
//output byte are calculated break and move to next pdm output byte.

//Oversample current pcm sample 'pdm_ovr' times.
        pdm_conv_pcm_smpl(sintbl[cur_smpl]);
        pdm_conv_pcm_smpl(sintbl[cur_smpl]);
//        pdm_conv_pcm_smpl(sintbl[cur_smpl]);
//        pdm_conv_pcm_smpl(sintbl[cur_smpl]);
//Increment to next pcm sample in sine table.
        ++cur_smpl;
      }
//Update amount in buffer.
      ++pdm_buf_amt;
    }

    if(pdm_uart_tx_buf_empty()) {
//UART ready for another 8 bits of pdm data.
      pdm_uart_tx_buf_wr();
    }
  }
#endif
}

//
//pdm_uart_test()
// This is a test/experiment to determine how UART speed corresponds to
// oversampling.
//
void pdm_uart_test(uint8_t fnidx) {
#ifdef PDM_UART_TEST
  uint8_t smpl;

  pcm_init(ADC_CLK_DIV_16);

  pdm_init(PDM_OVR_8X,
           ADC_CLK_DIV_16);

//  UBRR0 = 103;
//  UBRR0 = 1;
//  UBRR0 = 27; //For 8x oversampling. (good)
//  UBRR0 = 54; //For 4x oversampling. (good)
//  UBRR0 = 54; //For 8x oversampling.
//  pdm_ovr_set(8); //8x oversampling.

  pcm_start_conversion();
  pdm_uart_start();

  while(1) {
//Fill pdm buffer byte with samples.
    while(pdm_bit_cnv) {
//Pending sample read by adc.
      if(pcm_adc_pending()) {
//Clear pending bit.
        pcm_adc_clear_pending();
//Initialize flag indicating remaining oversamples.
        pdm_ovr_cnt = pdm_ovr;
//Read top 8 bits of sample discarding 2 least significant bits.
        smpl = pcm_adc_h_rd();
//Oversample byte.
        while(pdm_ovr_cnt) {
//Convert current sample.
          pdm_conv_pcm_smpl(smpl);
        }
      }
    }

//Ready to shift out the PDM byte.
    if(pdm_uart_tx_buf_empty()) {
//UART ready for another 8 bits of pdm data.
      pdm_uart_tx_buf_wr(pdm_buf);
//Reset the bit mask.
      pdm_bit_cnv = bit(0);
//Reset the current pdm buffer byte to all zero.
      pdm_buf = 0;
    }
  }
#endif
}

//
//pdm_test()
// Runs all tests in this file.
//
void pdm_test(void) {
#ifdef BUILD_UNIT_TESTS
  pdm_conv_pcm_smpl_test(0);
  pdm_ovr_set_test(1);
  pdm_sine_out_test(2);
  pdm_uart_test(3);
//  pdm_failed_test(0xFF,0xFF,0xFF);
#endif
}


#endif
