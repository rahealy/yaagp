///////////////////////////////////////////////////////////////////////
// main_test.h
//  Unit Tests for main file.
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef MAIN_TEST_H
#define MAIN_TEST_H

//
//main_failed_test()
// Called when a test has failed. Suggested usage is to set a debugger
// to break at this function and read the argument values fn, tst, and
// res to determine the result that failed. Examine the call stack to
// determine the exact line in the test function that failed.
// 
// fn - Function number set in main_test();
// tst - Test set number in function.
// res - Result number in test set.
//
// Enters infinite loop and does not return.
//
void main_failed_test(uint8_t fn, uint8_t tst, uint8_t res) {
#ifdef BUILD_UNIT_TESTS
  while(1){}
#endif
}

//
//main_test()
// Runs all tests in this file.
//
void main_test(void) {
#ifdef BUILD_UNIT_TESTS
//  failed_test(0xFF,0xFF,0xFF);
#endif
}


#endif
