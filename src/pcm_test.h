///////////////////////////////////////////////////////////////////////
// pcm_test.h
//  Unit Tests for functions in pdm.h. 
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef PCM_TEST_H
#define PCM_TEST_H

//
//pcm_failed_test()
// Called when a test has failed. Suggested usage is to set a debugger
// to break at this function and read the argument values fn, tst, and
// res to determine the result that failed. Examine the call stack to
// determine the exact line in the test function that failed.
// 
// fn - Function number set in main_test();
// tst - Test set number in function.
// res - Result number in test set.
//
// Enters infinite loop and does not return.
//
void pcm_failed_test(uint8_t fn, uint8_t tst, uint8_t res) {
#ifdef BUILD_UNIT_TESTS
  while(1){}
#endif
}

void pcm_adc_test(uint8_t fnidx) {
#ifdef PCM_ADC_TEST
  uint8_t cur_ovr = 0; //Counter for oversample amount.
  pcm_smpl_t pcm_smpl;

  pcm_init(ADC_CLK_DIV_16); //9.6kHz sample rate.
  pdm_init(ADC_CLK_DIV_16); //9.6kHz sample rate.
  pdm_ovr_set(1);

  UBRR0 = 832;

  pcm_start_conversion();

  while(1) {
    if(pcm_adc_pending()) {
//Load sample.
      pcm_smpl = pcm_adc_l_rd();
//Clear pending flag for next read.
      pcm_adc_clear_pending();

      if(pdm_buf_amt < PDM_BUF_T_LEN) {
//      for(cur_ovr = pdm_ovr; cur_ovr; --cur_ovr) {
//Convert PCM sample to PDM sample using oversample.
        pdm_conv_pcm_smpl(pcm_smpl);//pcm_smpl);
//      }
      }
    }
  
    if(0 == pdm_bit_cnv) {
//End of current pdm byte. Reset the bit mask.
      pdm_bit_cnv = bit(0);
//Update PDM buffer write position.
      pdm_wr_idx = pdm_idx4_inc(pdm_wr_idx);
//Reset the current pdm buffer byte to all zero.
      pdm_buf[pdm_wr_idx] = 0;
//Update amount in PDM buffer.
      ++pdm_buf_amt;
    }

    if(pdm_uart_tx_buf_empty()) {
//UART ready for another 8 bits of pdm data.
      if(pdm_buf_amt) {
        pdm_uart_tx_buf_wr(pdm_buf[pdm_rd_idx]);
        pdm_rd_idx = pdm_idx4_inc(pdm_rd_idx);
        --pdm_buf_amt;
      }
    }
  }
#endif
}

//
//pcm_test()
// Runs all tests in this file.
//
void pcm_test(void) {
#ifdef BUILD_UNIT_TESTS
  pcm_adc_test(0);
//  pcm_failed_test(0xFF,0xFF,0xFF);
#endif
}

#endif
