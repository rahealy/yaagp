////////////////////////////////////////////////////////////////////////////////
// pcm.h
//  Routines for managing the PCM samples output by the ADC on the arduino UNO.
////////////////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef PCM_H
#define PCM_H

////////////////////////////////////////////////////////////////////////////////
// ADC Defines
////////////////////////////////////////////////////////////////////////////////

//ADCSRA Bits
#define PCM_ADCSRA_ADEN  bit(ADEN)  //0b10000000 //ADC Enable
#define PCM_ADCSRA_ADSC  bit(ADSC)  //0b01000000 //ADC Start Conversion
#define PCM_ADCSRA_ADATE bit(ADATE) //0b00100000 //ADC Auto Trigger Enable
#define PCM_ADCSRA_ADIF  bit(ADIF)  //0b00010000 //ADC Interrupt flag set when sample ready.
#define PCM_ADCSRA_ADIE  bit(ADIE)  //0b00001000 //ADC Interrupt Enable

//ADMUX Bits
#define PCM_ADMUX_AVCC  bit(REFS0) //0b01000000 //Use power supply voltage for ref.
#define PCM_ADMUX_ADLAR bit(ADLAR) //0b00100000 //AD left adjust.

//ADCSRB Bits
#define PCM_ADCSRB_ACME 0b01000000 //Analog Comparator Multiplexer Enable


////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

//
//pcm_start_conversion()
// Start the conversion by writing 1 to ADSC bit.
//
inline_static_fn void pcm_start_conversion() {
  ADCSRA |= PCM_ADCSRA_ADSC; //Start conversion.
}

//
//Returns non-zero if ADC interrupt flag is set indicating a pending sample.
//
inline_static_fn uint8_t pcm_adc_pending(void) {
  return (ADCSRA & PCM_ADCSRA_ADIF);
}

//
//pcm_pending_clear()
// Clear the interrupt flag by writing 1 to ADIF bit.
//
inline_static_fn void pcm_adc_clear_pending(void) {
  ADCSRA = ADCSRA;
//  ADCSRA |= PCM_ADCSRA_ADIF;
}

//
//Read low byte of ADC sample buffer.
//
inline_static_fn uint8_t pcm_adc_l_rd(void) {
  return ADCL;
}

//
//Read high byte of ADC sample buffer.
//
inline_static_fn uint8_t pcm_adc_h_rd(void) {
  return ADCH;
}

//
//pcm_set_clk_div()
// Set ADC clock divisor.
//
inline_static_fn void pcm_set_clk_div(adc_clk_div clkdiv) {
  switch(clkdiv) {
    case ADC_CLK_DIV_16:
      ADCSRA |= bit(ADPS2); 
    break;

    case ADC_CLK_DIV_32:
      ADCSRA |= bit(ADPS0) | bit(ADPS2);
    break;

    case ADC_CLK_DIV_64:
      ADCSRA |= bit(ADPS1) | bit(ADPS2);
    break;

    default:
    case ADC_CLK_DIV_128:
      ADCSRA |= bit(ADPS0) | bit(ADPS1) | bit(ADPS2);
    break;
  }
}

//
//pcm_set_clk_div()
// Set ADC clock divisor.
//
inline_static_fn adc_clk_div pcm_get_clk_div(void) {
  uint8_t bits = ADCSRA & (bit(ADPS0) | bit(ADPS1) | bit(ADPS2));

  switch(bits) {
    case (uint8_t) (bit(ADPS2)):
    return ADC_CLK_DIV_16;

    case (uint8_t) (bit(ADPS0) | bit(ADPS2)): 
    return ADC_CLK_DIV_32;

    case (uint8_t) (bit(ADPS1) | bit(ADPS2)): 
    return ADC_CLK_DIV_64;

    case (uint8_t) (bit(ADPS0) | bit(ADPS1) | bit(ADPS2)):
    return ADC_CLK_DIV_128;
    
    default:
    break;
  }

  return 0;
}

//
//pcm_init()
// Set up the ADC based on frequency corresponding to clock divisor.
// Default is 128.
//
inline_static_fn void pcm_init(adc_clk_div clkdiv) {
  DIDR0  = 0b00111111;                        //Digital inputs disabled
  ADMUX  = PCM_ADMUX_AVCC | PCM_ADMUX_ADLAR;  //Use AVcc as reference, left adjusted output.

  ADCSRA = PCM_ADCSRA_ADEN  | //Enable ADC
           PCM_ADCSRA_ADATE;  //Auto triger enable

  pcm_set_clk_div(clkdiv);

  ADCSRB = PCM_ADCSRB_ACME; //Enable multiplexer
}


#endif //#ifndef PCM_H
