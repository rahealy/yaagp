///////////////////////////////////////////////////////////////////////
// main.c
//  Yet Another Arduino Guitar Pedal. (yaagp)
//
//  Code takes PCM samples from ADC input A0, processes it and outputs
//  as pulse decimated modulation (PDM) bit stream from the UART SPI Tx
//  pin.
//
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Compile Options
//  Define these in a gcc invocation using the -D flag eg.
//  avr-gcc -DUSE_INLINE_STATIC_FNS
//
//
// USE_INLINE_STATIC_FNS
//  If defined inline functions beginning with 'inline_static_fn'  
//  Experimental.
//
//#define USE_INLINE_STATIC_FNS
//
//
// USE_REGISTERS
//  If defined as many global variables as will fit will be explicitly
//  stored in CPU registers rather than the slower stack. Experimental.
//
//#define USE_REGISTERS
//
//
// BUILD_UNIT_TESTS
//  If defined build unit tests. Will run tests instead of intended 
//  functions.
//
//#define BUILD_UNIT_TESTS
//
//
// BUILD_DEBUGGING
//  If defined build debugging functions.
//
//#define BUILD_DEBUGGING
//
/////////////////////////////////////////////////////////////////////


//
// Design Notes:
//
// Input and Output sample rates, clock divisors, maximum estimated bit
// depth given a 16MHz system clock:
//
//  76.9 kHz, (clk÷16),  >8 bit / 2
//  38.4 kHz, (clk÷32),  >9 bit / 2
//  19.2 kHz, (clk÷64),  >10 bit / 2
//   9.6 kHz, (clk÷128),  10 bit / 2
//
// ADC sampling rate is estimated using the following formula:
//
//  smplrt = 16000000/(divisor * 13)
//
// Bit depth is estimated based on the number of system clock ticks it
// takes to reproduce 1 sample. Given:
//
//  tcks_per_smpl = (16000000 / smplrt)
//
// A 76.9 kHz sampling rate requires ~208 ticks per sample. Since an 
// 8 bit sample represents 256 possible values we lose 48 values 
// (256 - 208) to rounding errors. So at a 16MHz clock, a 76.9 kHz 
// sampling rate we end up with absolutely best case sub 8 bit playback
// resolution.
//
// In reality the bit shifter takes at least 2 ticks to output one bit
// so the effective playback resolution is half the bit depth. Using 
// PDM spreads the rounding errors across many samples so improves the
// perceived audio quality somewhat.
//
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifdef USING_ARDUINO_IDE
 #include "Arduino.h"
#else
 #include <avr/io.h>
 #include <avr/interrupt.h>
 #include <avr/pgmspace.h>
 #include <avr/sleep.h>
 #include <util/delay.h>
#endif

#include "globals.h"
#include "tables.h"
#include "pcm.h"
#include "pdm.h"
#include "timer1.h"
#include "led.h"

////////////////////////////////////////////////////////////////////////////////
// Structures
////////////////////////////////////////////////////////////////////////////////

//
//Guitar pedal state.
//
typedef enum {
  GP_STATE_DLY,
  GP_STATE_TBL,
  GP_STATE_THRU,
  GP_STATE_TONE,
} gp_state;

//
//guitar_pedal
// 8 bit values are defined in globals.h as registers. 16 bit values are defined
// here so the compiler can deal with them.
//
typedef struct _guitar_pedal {
//Used globally.
  uint16_t wr_idx;     //Write index into pdm_buf delay buffer.
  uint16_t rd_idx;     //Read index into pdm_buf delay buffer.
  uint16_t tbl_rd_idx; //Read index into pdm_buf determined by table.
  uint16_t tbl_idx;    //Current index into modulation table.
  uint8_t  t_elpsd;    //Number of timer overflows.

//Adjustable effects parameters. FIXME: Needs save/load functionality.

//Table based modulation.
  uint8_t  tbl_dpth;   //Scales table's value's effect.
  uint8_t  tbl_mix;    //Table modulated delay mix of wet/dry. 255 wet, 127.5 50/50, 0 dry.
  uint8_t  tbl_fdbk;   //Table modulated delay feedback. 0 none, 255 max. 
  uint8_t  tbl_spd;    //Speed of table modulation in timer overflows per second.
  
//Delay.
  uint16_t dly_amt;    //Number of samples to delay
  uint8_t  dly_fdbk;   //Delay feedback. 0 none, 255 max.
  uint8_t  dly_mix;    //Mix of wet/dry. 255 wet, 127.5 50/50, 0 dry.
  
} guitar_pedal;

//
//gp{}
// Global pedal structure.
//
guitar_pedal gp = {
  .wr_idx     = 0,
  .rd_idx     = 0,
  .tbl_idx    = 0,
  .t_elpsd    = 0,

  .tbl_dpth   = 255,
  .tbl_mix    = 255,
  .tbl_fdbk   = 127,
  .tbl_spd    = 31,
  .tbl_rd_idx = 0,

  .dly_amt    = 0,
  .dly_fdbk   = 127,
  .dly_mix    = 127,
};


////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

//
// oversample_pcm_x8()
//  Oversample the PCM sample by 8x. In theory we'd oversample by 256x
//  since an 8-bit PCM sample has 256 unique values or steps. This takes
//  too many resources so a 256 step accumulator is used to spread the
//  error. At least I think that's how the math works...
//
inline_static_fn uint8_t oversample_pcm_x8(uint8_t a) {
  uint8_t wrb    = 1;
  uint8_t retval = 0;
  asm (
    " ADD  %[acc], %[pcm] \n" //  Add PCM to accumulator.
    " BRCS .+2            \n" //  Skip next instruction if carry set.
    " OR   %[out], %[wrb] \n" //  Carry not set. Set current bit.
    " ADD  %[wrb], %[wrb] \n" //  Rotate write bit left.
    " BRNE .-10           \n" //  If more bits to write jump to oversample, else fall through.
  : [wrb]  "+r" (wrb),
    [acc]  "+r" (pdm_acc_a),
    [pcm]  "+r" (a),
    [out]  "+r" (retval)
  :
  :
  );
  return retval;
}

//
// oversample_and_mix_x4()
//  Oversample the PCM samples by 4x. In theory we'd oversample by 256x
//  since an 8-bit PCM sample has 256 unique values or steps. This takes
//  too many resources so a 256 step accumulator is used to spread the
//  error. At least I think that's how the math works...
//
inline_static_fn uint8_t oversample_and_mix_x4(uint8_t a, uint8_t b) {
  uint8_t wrb    = 1; //Bit mask used to set current output bit.
  uint8_t retval = 0; //Output byte.
  asm (
    " ADD  %[acc], %[pcm_a] \n" //  Add PCM to accumulator.
    " BRCS .+2              \n" //  Skip next instruction if carry set.
    " OR   %[out], %[wrb]   \n" //  Carry not set. Set current bit.
    " ADD  %[wrb], %[wrb]   \n" //  Rotate write bit left.
    " ADD  %[acc], %[pcm_b] \n" //  Add PCM to accumulator.
    " BRCS .+2              \n" //  Skip next instruction if carry set.
    " OR   %[out], %[wrb]   \n" //  Carry not set. Set current bit.
    " ADD  %[wrb], %[wrb]   \n" //  Rotate write bit left.
    " BRNE .-18             \n" //  If more bits to write jump to oversample, else fall through.
  : [wrb]   "+r" (wrb),
    [acc]   "+r" (pdm_acc_a),
    [pcm_a] "+r" (a),
    [pcm_b] "+r" (b),
    [out]   "+r" (retval)
  :
  :
  );
  return retval;
}

//
//fixdiv8_trunc()
// Multiplies an 8 bit value by an 8 bit fraction (fract/256) and 
// truncates to whole.
//
inline_static_fn uint8_t fixdiv8_trunc(uint8_t whole, uint8_t fract) {
  uint8_t retval = 0;
  asm (
    " MUL %[whl], %[frc] \n" //Multiply value by fraction.
    " MOV %[ret], r1     \n" //Put result into return value.
  : [whl] "+r" (whole),
    [frc] "+r" (fract),
    [ret] "+r" (retval)
  :
  : "r0", "r1"
  );
  return retval;
}

//
//fixdiv_16x8_trunc()
// Multiplies a 16 bit value by an 8 bit fraction (fract/256) and 
// truncates to whole.
//
inline_static_fn uint16_t fixdiv_16x8_trunc(uint16_t whole, uint8_t fract) {
  uint16_t retval = 0;
  asm (
    " MUL  %A[whl], %[frc] \n" //Multiply least significant byte of whole by fraction.
    " MOV  %A[ret], r1     \n" //Put most significant result into first byte of retval.
    " MUL  %B[whl], %[frc] \n" //Multiply most significant byte of whole by fraction.
    " ADD  %A[ret], r0     \n" //Add result in r0 to low byte of retval.
    " ADC  %B[ret], r1     \n" //Add result in r1 to to high byte of retval with carry.
  : [whl] "+r" (whole),
    [frc] "+r" (fract),
    [ret] "+r" (retval)
  :
  : "r0", "r1"
  );
  return retval;
}

//
//state_thru()
// Process without any effects.
//
inline_static_fn void state_thru(void) {
  pdm_smpls = 0;
  pcm_smpl  = 0;

  while(1) {
    if(pcm_adc_pending()) {
//Read top 8 bits of PCM sample discarding 2 least significant bits.
      pcm_smpl = pcm_adc_h_rd();
//Clear the pending bit.
      pcm_adc_clear_pending();
    }

    if(pdm_uart_tx_buf_empty()) {
//Send another 8 PDM samples.
      pdm_uart_tx_buf_wr(pdm_smpls);
//Calculate next 8 pdm samples.
      pdm_smpls = oversample_pcm_x8(pcm_smpl);
    }
  }
}

//
//state_tone()
// Output a sinewave.
//
inline_static_fn void state_tone(void) {
  pdm_smpls = 0;
  pcm_smpl  = 0;

  for(;;) {
    if(pcm_adc_pending()) {
//Read and dispose of the top 8 bits.
      pcm_smpl = pcm_adc_h_rd();
//Clear the pending bit.
      pcm_adc_clear_pending();
//Load the sample from the table.
      pcm_smpl = lod_sin8(tbl_idx_l);
      ++tbl_idx_l;
      tbl_idx_h = 0xFF;
    }

    if(pdm_uart_tx_buf_empty()) {
      if(tbl_idx_h) {
//Send another 8 PDM samples.
        pdm_uart_tx_buf_wr(pdm_smpls);
//Calculate next 8 pdm samples.
        pdm_smpls = oversample_pcm_x8(pcm_smpl);
//Decrement.
        --tbl_idx_h;
      }
      
    }
  }
}


//
//process_tbl()
// Given a sample update table driven delay buffer with feedback
// mix wet/dry, update write and read indices.  
//
inline_static_fn uint8_t process_tbl(uint8_t smpl) {
  uint8_t retval;

//Mix sample corresponding to table with current sample and store in delay buffer.
//   pdm_buf[gp.wr_idx] = fixdiv8_trunc(pdm_buf[gp.tbl_rd_idx], gp.tbl_fdbk) + 
//                        fixdiv8_trunc(smpl, ~gp.tbl_fdbk);

  pdm_buf[gp.wr_idx] = smpl;
  retval = fixdiv8_trunc(pdm_buf[gp.tbl_rd_idx], gp.tbl_fdbk) + 
                         fixdiv8_trunc(smpl, ~gp.tbl_fdbk);

//Mix wet/dry signal and use for output.
//   retval = fixdiv8_trunc(pdm_buf[gp.wr_idx], gp.tbl_mix) +
//            fixdiv8_trunc(smpl, ~gp.tbl_mix);

//Increment write index. Roll over.
  gp.wr_idx     = (gp.wr_idx + 1) & 0x03FF;
  gp.tbl_rd_idx = (gp.tbl_rd_idx + 1) & 0x03FF;

//Update read index.
  gp.t_elpsd += timer1_elapsed();

  if(timer1_overflow()) {
    timer1_overflow_clear();

    ++gp.t_elpsd;
    if(gp.t_elpsd == gp.tbl_spd) {
//Move read pointer to next position determined by table.
      gp.tbl_idx = (gp.tbl_idx + 1) & 0x03FF;
      gp.tbl_rd_idx = (gp.wr_idx + lod_sin16(gp.tbl_idx)) & 0x03FF;
//      gp.tbl_rd_idx = (gp.wr_idx + fixdiv_16x8_trunc(lod_sin(gp.tbl_idx), gp.tbl_dpth)) & 0x03FF;
      gp.t_elpsd = 0;
    }
  }
  return retval;
}

//
//state_tbl()
// Process guitar with table driven delay effects.
//
inline_static_fn void state_tbl(void) {
  pdm_smpls     = 0;
  pcm_smpl      = 0;
  gp.t_elpsd    = 0;
  gp.wr_idx     = 0;
  gp.rd_idx     = 0;
  gp.tbl_rd_idx = 0;
  gp.tbl_idx    = 0;

  for(;;) {
    if(pcm_adc_pending()) {
//Read top 8 bits of PCM sample discarding 2 least significant bits.
      pcm_smpl = pcm_adc_h_rd();
//Clear the pending bit.
      pcm_adc_clear_pending();
//Process next table driven delay sample.
      pcm_smpl = process_tbl(pcm_smpl);
    }

    if(pdm_uart_tx_buf_empty()) {
//Send another 8 PDM samples.
      pdm_uart_tx_buf_wr(pdm_smpls);
//Calculate next 8 pdm samples.
      pdm_smpls = oversample_pcm_x8 (pcm_smpl);
    }
  }

  pdm_uart_stop();
}

int main(void) {
  globals_init();
  led_init();
  pcm_init(ADC_CLK_DIV_128);
  pdm_init(ADC_CLK_DIV_128);
//  timer1_init(TIMER1_CLK_DIV_1);
  pcm_start_conversion();
  pdm_uart_start();
//  state_thru();
  state_tone();
//   state = GP_STATE_TONE;
// 
//   for(;;) {
//     switch(state) {
//       case GP_STATE_TONE:
//         state_tone();
//       break;
// 
//       case GP_STATE_THRU:
//         state_thru();
//       break;
// 
//       case GP_STATE_TBL:
//         state_tbl();
//       break;
//     }
//   }
  
  return 0;
}

// //
// //process_dly()
// // Given a sample in pcm_smpl update delay buffer with feedback, mix 
// // wet/dry, update write and read indices.
// //
// // Return result in pcm_smpl.
// //
// inline_static_fn uint8_t process_dly(uint8_t smpl) {
//   uint8_t retval;
// //Mix delayed sample with current sample and store in delay buffer.
//   pdm_buf[gp.wr_idx] = fixdiv8_trunc(pdm_buf[gp.rd_idx], gp.dly_fdbk) + 
//                        fixdiv8_trunc(pcm_smpl, ~gp.dly_fdbk);
// //Mix wet/dry signal and use for output.
//   retval = fixdiv8_trunc(pdm_buf[gp.wr_idx], gp.dly_mix) +
//            fixdiv8_trunc(pcm_smpl, ~gp.dly_mix);
// //Increment write and read index.
//   gp.wr_idx = (gp.wr_idx + 1) & 0x03FF;
//   gp.rd_idx = (gp.rd_idx + 1) & 0x03FF;
// //Return result.
//   return retval;
// }
//
// //
// //state_dly()
// // Process guitar with delay effects.
// //
// inline_static_fn void state_dly(void) {
//   pdm_smpls = 0;
//   gp.wr_idx = 0;
//   gp.tbl_rd_idx = 0;
//   gp.rd_idx = gp.dly_amt;
// 
//   pdm_uart_set_ubrr0 (
//     PDM_CHANS_x2,
//     pcm_get_clk_div()
//   );
// 
//   for(;;) {
//     if(pcm_adc_pending()) {
// //Read top 8 bits of PCM sample discarding 2 least significant bits.
//       pcm_smpl = pcm_adc_h_rd();
// //Clear the pending bit.
//       pcm_adc_clear_pending();
// //Process next delay sample.
//       pcm_smpl = process_dly(pcm_smpl);
//     }
// 
//     if(pdm_uart_tx_buf_empty()) {
// //Send another 8 PDM samples.
//       pdm_uart_tx_buf_wr(pdm_smpls);
// //Calculate next 8 pdm samples.
//       pdm_smpls = oversample_pcm_x8 (pcm_smpl);
//     }
//   }
// }

// //
// //state_flange()
// // Process guitar with flange effect.
// //
// inline_static_fn void state_flange(void) {
//   gp.wr_idx  = 0;
//   gp.rd_idx  = 0;
//   gp.tbl_idx = 0;
//   gp.t_elpsd   = 0;
// 
//   pdm_uart_set_ubrr0 (
//     PDM_CHANS_x2,
//     pcm_get_clk_div()
//   );
// 
//   while(1) {
//     if(pcm_adc_pending()) {
// //Increment write/read index.
//       gp.wr_idx = (gp.wr_idx + 1) & 0x03FF;
//       gp.rd_idx = (gp.rd_idx + 1) & 0x03FF;
// 
// //Read top 8 bits of PCM sample discarding 2 least significant bits.
// //      pdm_buf[gp.wr_idx] = pcm_adc_h_rd();
//       pdm_buf[gp.wr_idx] = fixdiv8_trunc(pdm_buf[gp.rd_idx], gp.dly_fdbk) + pcm_adc_h_rd();
// 
// //Clear the pending bit.
//       pcm_adc_clear_pending();
//     }
// 
//     if(pdm_uart_tx_buf_empty()) {
// //Send another 8 PDM samples.
//       pdm_uart_tx_buf_wr(pdm_smpls);
// 
// // To provide time based delay sweep use elapsed time to determine the 
// // offset into the read pointer table. The table value is used to determine
// // offset from the write index in the delay buffer.
//       gp.t_elpsd += timer1_elapsed();
//       if(gp.t_elpsd > gp.t_step) {
//         gp.t_elpsd   -= gp.t_step;
//         gp.tbl_idx  = (gp.tbl_idx + 1) & 0x03FF;
//         gp.rd_idx   = (fixdiv8_trunc(lod_sin(gp.tbl_idx), gp.flg_dpth) + gp.wr_idx) & 0x03FF;
//       }
// 
// //Calculate next 8 pdm samples.
//       pdm_smpls = oversample_and_mix_x4 (
//         pdm_buf[gp.wr_idx], 
//         pdm_buf[gp.rd_idx]
//       );
//     }
//   }
// }
