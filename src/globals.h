///////////////////////////////////////////////////////////////////////
// globals.h
//  Contains all global variables. Include this first.
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef GLOBALS_H
#define GLOBALS_H

////////////////////////////////////////////////////////////////////////////////
// Global Defines
////////////////////////////////////////////////////////////////////////////////

//
//USE_INLINE_STATIC_FNS
// If defined will inline functions beginning with 'inline_static_fn'
//
#ifdef USE_INLINE_STATIC_FNS
#define inline_static_fn inline static
#else
#define inline_static_fn
#endif

//
//bit(b)
// Set bit b in a byte and return as constant.
//
#ifndef bit 
#define bit(b) ((uint8_t) 1 << (b))
#endif

//
//PDM_OVR_*
// Oversample rates for setting pdm_ovr_cnt shift register.
//
typedef enum {
  PDM_CHANS_x1   = 0,  //One pdm channel, no mix.
  PDM_CHANS_x2   = 1,  //Mixing two PDM channels.
  PDM_CHANS_NUM,      //Number of channel options.
} pdm_num_chans;

#define PDM_CHANS_DEFAULT PDM_CHANS_x1

//
// adc_clk_div
//  ADC Clock divisor bits. Assumes 16MHz system clock.
//
//  Formula used to calculate sample rate:
//   smplrt = FOSC/(div * 13)
//
typedef enum {
  ADC_CLK_DIV_16  = 0, //76.9 kHz, (clk÷16)
  ADC_CLK_DIV_32  = 1, //38.4 kHz, (clk÷32)
  ADC_CLK_DIV_64  = 2, //19.2 kHz, (clk÷64)
  ADC_CLK_DIV_128 = 3, // 9.6 kHz, (clk÷128)
  ADC_CLK_DIV_NUM      //Number of oversample rates.
} adc_clk_div;

#define ADC_CLK_DIV_DEFAULT ADC_CLK_DIV_128


//
// timer1_clk_div
//  Clock source divisors.
//
typedef enum {
  TIMER1_CLK_DIV_0    = 0,
  TIMER1_CLK_DIV_1    = 1,
  TIMER1_CLK_DIV_8    = 2, //Timer1 overflows at a rate of 30.5Hz [(16000000 / 8) / 65536]
  TIMER1_CLK_DIV_64   = 3,
  TIMER1_CLK_DIV_256  = 4,
  TIMER1_CLK_DIV_1024 = 5,
  TIMER1_CLK_DIV_NUM  //Number of clock divisor rates.
} timer1_clk_div;

#define TIMER1_CLK_DIV_DEFAULT TIMER_CLK_DIV_128


////////////////////////////////////////////////////////////////////////////////
// Global Variables
//
// AVR opcodes with immediates only work on registers 16-31. Regs 18-up are
// reserved by the C ABI for passing variables to functions and local function
// variables. As a result all variables manipulated with immediates are put in
// r16 or r17 so the compiler doesn't waste cycles moving them.
////////////////////////////////////////////////////////////////////////////////

register uint8_t pdm_acc_a   asm("r6");  //Global PDM accumulator for PCM->PDM conversion.
register uint8_t pdm_acc_b   asm("r7");  //Global PDM accumulator for PCM->PDM conversion.
register uint8_t pcm_smpl    asm("r8");  //PCM sample.
register uint8_t pdm_smpls   asm("r9");  //8 PDM samples.
register uint8_t state       asm("r10"); //State.
register uint8_t tbl_idx_l   asm("r11"); //8 bit entry table index.
register uint8_t tbl_idx_h   asm("r12"); //16 bit entry table index.

//
//pdm_buf[]
// Buffer stores pdm samples for use in delay based effects. 1024 byte
// aligned to make pointer wrap easier.
//
#define PDM_BUF_LEN 1024
uint8_t pdm_buf[PDM_BUF_LEN] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

////////////////////////////////////////////////////////////////////////////////
// Global Inline Functions
////////////////////////////////////////////////////////////////////////////////


inline_static_fn void globals_init(void) {
    pdm_acc_a = 0;
    pdm_acc_b = 0;
    pcm_smpl  = 0; 
    pdm_smpls = 0;
    state     = 0;
}

#endif //#ifndef GLOBALS_H
