///////////////////////////////////////////////////////////////////////
// pdm.h
//  Routines for using the UART in SPI mode to output pdm audio data.
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef PDM_H
#define PDM_H

#define PDM_DDRD_TXD       bit(PD1) //TX IO port D is a digital output.

#define PDM_UCSR0A_UDRE0   bit(UDRE0) //TX buffer register empty flag.
#define PDM_UCSR0A_TXC0    bit(TXC0)  //Transmit complete flag.
#define PDM_UCSR0A_RXC0    bit(RXC0)  //Receive complete flag.

#define PDM_UCSR0B_RXEN0   bit(RXEN0) //Enable Rx
#define PDM_UCSR0B_TXEN0   bit(TXEN0) //Enable Tx

#define PDM_UCSR0C_UMSEL01 bit(UMSEL01) //UART function selection bit.
#define PDM_UCSR0C_UMSEL00 bit(UMSEL00) //UART function selection bit.
#define PDM_USCR0C_UDORD0  bit(UDORD0)  //UART data order (MSB first, LSB first).
#define PDM_UCSR0C_UCPHA0  bit(UCPHA0)  //UART phase.
#define PDM_UCSR0C_UCPOL0  bit(UCPOL0)  //UART polarity.


////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

//
//pdm_uart_tx_buf_empty()
// True if UART TX buffer is empty and can be written to.
//
inline_static_fn uint8_t pdm_uart_tx_buf_empty(void) {
  return (UCSR0A & PDM_UCSR0A_UDRE0);
}

//
//pdm_uart_tx_complete()
// True if UART TX is complete.
//
inline_static_fn uint8_t pdm_uart_tx_complete(void) {
  return (UCSR0A & PDM_UCSR0A_TXC0);
}

//
//pdm_uart_tx_buf_wr()
// Write 8 bits of pdm data from the pdm buffer to tx buffer of the 
// UART SPI and increment read index to next buffer entry.
//
inline_static_fn void pdm_uart_tx_buf_wr(uint8_t dat) {
  UDR0 = dat; 
}

//
//pdm_uart_stop()
// Stop the UART.
//
inline_static_fn void pdm_uart_stop(void) {
//Stop uart.
  UCSR0B &= (~PDM_UCSR0B_TXEN0);
//Wait for last byte to be sent.
  while(!pdm_uart_tx_complete()) {}
}

//
//pdm_uart_start()
// Start the UART. Does nothing if UART is already started.
//
inline_static_fn void pdm_uart_start(void) {
  UCSR0B |= PDM_UCSR0B_TXEN0;
}

//
//pdm_uart_set_ubrr0()
// Wait until all bits shifted out.
//
inline_static_fn void pdm_uart_set_ubrr0(uint8_t chans, uint8_t div) {
  UBRR0 = lod_ubrr0(chans,div);
}

//
//pdm_init()
// Initalize the UART in SPI mode for use as a double buffered bit
// shifter to output PDM audio data.
//
// Given:
//
//  baud   - Baud rate of the UART in bits per second.
//  div    - Divsor corresponding to ADC. One of 16, 32, 64, or 128.
//  FOSC   - System clock set to 16000000
//  pre    - Prescaler value (UBRR0)
//  smplrt - ADC sample rate for a given divisor.
//
// Baud is calculated based on ADC sample rate (from ATmega328p 
// datasheet):
//
//  baud = smplrt = FOSC / (div * 13)
//
// Prescaler value in UBRR0 is calculated using:
//
//  pre = (FOSC / (2 * baud)) - 1
//
// Combined and simplified:
//
//  pre = (FOSC / (2 * (FOSC / (div * 13)))) - 1
//  pre = ((13 * div) / 2) - 1
//
inline_static_fn void pdm_init(adc_clk_div clkdiv) {
  UBRR0 = 0;                     //Reset.
  DDRD |= PDM_DDRD_TXD;          //Set TX to output.
  UCSR0C = PDM_UCSR0C_UMSEL01 |  //Master SPI
           PDM_UCSR0C_UMSEL00 |  //Master SPI
           PDM_USCR0C_UDORD0  |  //Data order, LSB (bit 0) transmitted first.
           PDM_UCSR0C_UCPHA0  |  //Clock phase (may be either.)
           PDM_UCSR0C_UCPOL0;    //Clock polarity (may be either.)
//  UBRR0 = 25;//lod_ubrr0(PDM_CHANS_x1, div);
  UBRR0 = 50;//lod_ubrr0(PDM_CHANS_x1, div);
}

#endif //#ifndef PDM_H
