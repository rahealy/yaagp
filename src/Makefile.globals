#######################################################################
# Makefile.globals
#  Global variables used by all makefiles. No targets.
#######################################################################

# 
# MIT License
# 
# Copyright (c) 2020 Richard Healy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 

SRCDIR = .
BUILDDIR = .
PREFIX = .

#######################################################################
#ARDUINO_IDE_DIR - Installation of arduino IDE for build tools.
#######################################################################

ARDUINO_IDE_DIR = $(HOME)/local/arduino-1.8.13

#######################################################################
#ARDUINO_IDE_DEFINES - Defines mostly pulled from Arduino IDE output.
#                      Exception is -DUSING_ARDUINO_IDE which is used
#                      by code to determine whether to include 
#                      arduino.h or the standard gcc-avr includes.
#
# USING_ARDUINO_IDE - Used by software to determine what to include.
# ARDUINO           - Not sure. Was used in Arduino IDE.
# ARDUINO_AVR_UNO   - Board is an Arduino Uno.
# ARDUINO_ARCH_AVR  - Arduino UNO uses an AVR(r).
#######################################################################

ARDUINO_IDE_DEFINES  = -DUSING_ARDUINO_IDE
ARDUINO_IDE_DEFINES += -DARDUINO=10813
ARDUINO_IDE_DEFINES += -DARDUINO_AVR_UNO
ARDUINO_IDE_DEFINES += -DARDUINO_ARCH_AVR

# Arduino.h
ARDUINO_IDE_INCLUDES  = -I$(ARDUINO_IDE_DIR)/hardware/arduino/avr/cores/arduino
# Other headers
ARDUINO_IDE_INCLUDES += -I$(ARDUINO_IDE_DIR)/hardware/arduino/avr/variants/standard

#######################################################################
# AVR_IMG - Name of executable image.
#######################################################################

AVR_IMG = main$(EXT_SUFFIX)

AVR_IMGOUT = $(BUILDDIR)/$(AVR_IMG)
AVR_ELFOUT = $(AVR_IMGOUT).elf
AVR_HEXOUT = $(AVR_IMGOUT).hex

#######################################################################
# AVR_CPU - Arduino Uno R3 uses an atmega328.
# AVR_FREQUENCY - CPU frequency is 16 MHz.
#######################################################################

AVR_CPU       = atmega328p
AVR_FREQUENCY = 16000000

#######################################################################
# AVR_BINDIR - Various binutils (avr-gcc, avr-ld, etc.)
# AVR_GCC - Compiler executable.
# AVR_GDB - Debugger executable.
# AVR_OBJDUMP - Object dumper executable.
# AVR_OBJCOPY - Object copy and convert to hex.
#######################################################################

AVR_BINDIR  = $(ARDUINO_IDE_DIR)/hardware/tools/avr/bin
AVR_GCC     = $(AVR_BINDIR)/avr-gcc
AVR_GDB     = avr-gdb
AVR_OBJDUMP = $(AVR_BINDIR)/avr-objdump
AVR_OBJCOPY = $(AVR_BINDIR)/avr-objcopy

AVR_OBJDUMP_ARGS = --disassemble --file-headers --section-headers

#######################################################################
# AVR_DEFINES
#  F_CPU - CPU frequency.
#######################################################################

AVR_DEFINES = -DF_CPU=$(AVR_FREQUENCY)

#######################################################################
#GDB_COMMAND_FILE file used by avr-gdb -x $(GDB_COMMAND_FILE)
#######################################################################

GDB_COMMAND_FILE = gdbcommands.txt

#######################################################################
#AVR_OBJS - Target object files from source.
#######################################################################

AVR_OBJS = main$(EXT_SUFFIX).o
