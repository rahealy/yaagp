///////////////////////////////////////////////////////////////////////
// timer1.h
//  Routines for managing timer1 on the arduino UNO.
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef TIMER1_H
#define TIMER1_H

///////////////////////////////////////////////////////////////////////
// PWM on 16 bit timer 1. PWM.
///////////////////////////////////////////////////////////////////////

//PRR Bits - Power Reduction Register
#define PRR_PRTIM1 bit(PRTIM1) //Timer one bit in power reduction register.

//DDRB Bits -  Port B Data Direction Register
#define DDRB_DDB1 bit(DDB1)  // 0b00000010 //Digital pin 9, PORTB.PB1 (OC1A from PWM) 
#define DDRB_DDB2 bit(DDB2)  // 0b00000100 //Digital pin 10, PORTB.PB2 (OC1B from PWM)

//TCCR1A Bits - Timer Counter Control Register A
#define TCCR1A_COM1A0 bit(COM1A0) //0b01000000
#define TCCR1A_COM1A1 bit(COM1A1) //0b10000000
#define TCCR1A_COM1B0 bit(COM1B0) //0b00010000
#define TCCR1A_COM1B1 bit(COM1B1) //0b00100000
#define TCCR1A_WGM11  bit(WGM11)  //0b00000010

//TCCR1B Bits - Timer Counter Control Register B
#define TCCR1B_WGM12  bit(WGM12)  //0b00001000
#define TCCR1B_WGM13  bit(WGM13)  //0b00010000
#define TCCR1B_CS10   bit(CS10)   //0b00000001
#define TCCR1B_ICES1  bit(ICES1)  //0b01000000

//TIFR1 Bits
#define TIFR1_TOV1    bit(0)      //Timer/Counter1, Overflow Flag

//TIMSK Bits
#define TIMSK1_ICIE1  bit(ICIE1)  //Timer/Counter1, Input Capture Interrupt Enable
#define TIMSK1_OCIE1B bit(OCIE1B) //Timer/Counter1, Output Compare B Match Interrupt Enable
#define TIMSK1_OCIE1A bit(OCIE1A) //Timer/Counter1, Output Compare A Match Interrupt Enable
#define TIMSK1_TOIE1  bit(TOIE1)  //Timer/Counter1, Overflow Interrupt Enable

//Timer clock selection bits 3:0. CS10, CS11, CS12.
#define TCCR1B_OFF  (0)                     //Timer off
#define TCCR1B_1    (bit(CS10))             //No prescaling  (clkio÷1)
#define TCCR1B_8    (bit(CS11))             //From prescaler (clkio÷8)
#define TCCR1B_64   (bit(CS10) | bit(CS11)) //From prescaler (clkio÷64)
#define TCCR1B_256  (bit(CS12))             //From prescaler (clkio÷256)
#define TCCR1B_1024 (bit(CS10) | bit(CS12)) //From prescaler (clkio÷1024)


///////////////////////////////////////////////////////////////////////
// Timer1 Functions
///////////////////////////////////////////////////////////////////////

inline_static_fn void timer1_enable_interrupts() {
  TIMSK1 |= TIMSK1_TOIE1; // | TIMSK1_OCIE1A | TIMSK1_OCIE1B; // enable timer overflow interrupt ISR, Output Compare A Match ISR
}

inline_static_fn uint8_t timer1_overflow(void) {
  return (TIFR1 & TIFR1_TOV1);
}

inline_static_fn void timer1_overflow_clear(void) {
  TIFR1 |= TIFR1_TOV1;
}

//
//timer1_reset()
// Returns rough approximation of how many ticks since last call to elapsed.
//
inline_static_fn void timer1_reset(void) {
  TCNT1 = 0;
}

//
//timer1_elapsed()
// Returns rough approximation of how many ticks since last call to elapsed.
//
inline_static_fn uint16_t timer1_elapsed(void) {
  uint16_t now = TCNT1;
  TCNT1 = 0;
  return now;
}

//
//setup_timer1()
// Disables timer then sets it up. Requires call to timer1_enable() to 
// start again.
//
inline_static_fn void timer1_init(timer1_clk_div div) {
  TCCR1B = 0;

  switch(div) {
    case TIMER1_CLK_DIV_0:
      TCCR1B = TCCR1B_OFF;
    break;

    case TIMER1_CLK_DIV_1:
      TCCR1B = TCCR1B_1;
    break;

    case TIMER1_CLK_DIV_8:
      TCCR1B = TCCR1B_8;
    break;

    case TIMER1_CLK_DIV_64:
      TCCR1B = TCCR1B_64;
    break;

    case TIMER1_CLK_DIV_256:
      TCCR1B = TCCR1B_256;
    break;
    
    default:
    case TIMER1_CLK_DIV_1024:
      TCCR1B = TCCR1B_1024;
    break;
  }
}

/*
 * Timer ticks from 0-65535 at F_CPU speed.
 * On ADC interrupt OC1A is set to (time_cur - time_old) = sample duration / next_sample
 * OC1A is set 
 */
//
//setup_16bit_pwm_by_clkdiv()
// Set up the PWM based on frequency corresponding to clock divisor.
// Default is 128.
//
// duty - duty cycle 0-65535.
// div - clock divider.
//
//
// static inline void setup_16bit_pwm_by_clkdiv(int duty, timer1_clk_div div) {
//   TCCR1A = 0;
//   TCCR1B = 0;
//   
//   setup_timer1(div);
// 
//   switch(div) {
//     case CLK_DIV_1:
// //Nearest divisor to 1 is (clkio÷1) so divide top by 1.
//       pwm_div = 1;
//       pwm_cs = TCCR1B_1;
//     break;
// 
//     case CLK_DIV_16:
// //Nearest divisor to 16 is (clkio÷64) so divide top by 4.
//       pwm_div = 4;
//       pwm_cs = TCCR1B_64;
//     break;
// 
//     case CLK_DIV_32:
// //Nearest divisor to 32 is (clkio÷64) so divide top by 2.
//       pwm_div = 2;
//       pwm_cs = TCCR1B_64;
//     break;
// 
//     case CLK_DIV_64:
// //Nearest divisor to 64 is (clkio÷64) so divide top by 1.
//       pwm_div = 1;
//       pwm_cs = TCCR1B_64;
//     break;
//     
//     default:
// //Nearest divisor to 128 is (clkio÷256) so divide top by 2.
//       pwm_div = 2;
//       pwm_cs = TCCR1B_256;
//     break;
//   }
// 
// //  DDRB |=  DDRB_DDB1; //Output pin 9
// 
//   ICR1   = 0xFFFF / pwm_div; //Count from zero to ICR1 then reset to 0.
//   OCR1A  = duty / pwm_div;   //At zero output 1, count to OCR1A then output 0.
// 
//   TCCR1A |= TCCR1A_COM1A1 |  //Output 1 on zero, 0 on match (pin 9.)
//             TCCR1A_WGM11;    //Fast PWM, TOP = ICR1, OCR1x register updated at BOTTOM, TOV1 flag set at top. Mode #14.
//   TCCR1B |= TCCR1B_WGM12 |   //Fast PWM, TOP = ICR1, OCR1x register updated at BOTTOM, TOV1 flag set at top. Mode #14.
//             TCCR1B_WGM13;    //Fast PWM, TOP = ICR1, OCR1x register updated at BOTTOM, TOV1 flag set at top. Mode #14.
// }

#endif //#ifndef TIMER1_H
