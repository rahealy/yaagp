///////////////////////////////////////////////////////////////////////
// debugging.h
//  Provides interface with simulator to output debugging information.
///////////////////////////////////////////////////////////////////////

//
// MIT License
// 
// Copyright (c) 2020 Richard Healy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef DEBUGGING_H
#define DEBUGGING_H

#ifdef USING_SIMAVR

///////////////////////////////////////////////////////////////////////
// USING_SIMAVR
//  If defined assumes code is running in the SIMAVR simulator.
//
//  SIMAVR uses a section in the ELF formatted executable file to
//  implement various simulator functionality. Macros are defined in
//  SIMAVR's provided 'avr_mcu_section.h' to add information to the
//  ELF section which gets read by SIMAVR and acted upon.
///////////////////////////////////////////////////////////////////////

#include <avr_mcu_section.h>

//AVR_MCU_VCD_FILE - Tell SIMVAR the name of the VCD output file. 
//File "/proc/self/fd/1" is stdio.
AVR_MCU_VCD_FILE("trace.vcd", 1);

//SIMVAR_OUTPUT_PORTn - General purpose registers for debug output.
#define SIMAVR_OUTPUT_PORT0 GPIOR0
#define SIMAVR_OUTPUT_PORT1 GPIOR1
#define SIMAVR_OUTPUT_PORT2 GPIOR2

//AVR_MCU_SIMAVR_CONSOLE - Output port to console. This doesn't seem to
//work in all versions of simavr.
//AVR_MCU_SIMAVR_CONSOLE(&SIMAVR_OUTPUT_PORT2);

//AVR_MCU - Tell SIMVAR what to emulate.
AVR_MCU(16000000, "atmega328p");

//AVR_MCU_VOLTAGES - Tell SIMVAR what voltages to use.
AVR_MCU_VOLTAGES(5.0, 5.0, 5.0);

//AVR_MCU_VCD_PORT_PIN - Tell SIMVAR to output these pins to VCD file.
//AVR_MCU_VCD_PORT_PIN('D', 1, "UART_TX_PIN"); //UART TX pin 1 on the Arduino Uno board.

const struct avr_mmcu_vcd_trace_t _mytrace[]  _MMCU_ = {
//   { AVR_MCU_VCD_SYMBOL("ADCSRA"), .what = (void*)&ADCSRA, }, //.mask = bit(ADIF), 
//   { AVR_MCU_VCD_SYMBOL("ADC_CAPT"), .mask = DEBUGGING_ADC_CAPT_FLG, .what = (void*)&SIMAVR_OUTPUT_PORT0, },
//   { AVR_MCU_VCD_SYMBOL("TIMER1_OVF"), .mask = DEBUGGING_TIMER1_OVF_FLG, .what = (void*)&SIMAVR_OUTPUT_PORT0, },
//   { AVR_MCU_VCD_SYMBOL("TCNT1H"), .what = (void*)&TCNT1H, },
//   { AVR_MCU_VCD_SYMBOL("TCNT1L"), .what = (void*)&TCNT1L, },
   { AVR_MCU_VCD_SYMBOL("UART_TXBUF_EMPTY"), .mask = bit(UDRE0), .what = (void*)&UCSR0A, },
   { AVR_MCU_VCD_SYMBOL("UDR0"), .what = (void*)&UDR0, },   
   { AVR_MCU_VCD_SYMBOL("SIMAVR_OUTPUT_PORT0"), .what = (void*)&SIMAVR_OUTPUT_PORT0, },
   { AVR_MCU_VCD_SYMBOL("SIMAVR_OUTPUT_PORT1"), .what = (void*)&SIMAVR_OUTPUT_PORT1, },
   { AVR_MCU_VCD_SYMBOL("SIMAVR_OUTPUT_PORT2"), .what = (void*)&SIMAVR_OUTPUT_PORT2, },
};

inline_static_fn void debug_port0_out(uint8_t val) {
    SIMAVR_OUTPUT_PORT0 = val;
}

inline_static_fn void debug_port1_out(uint8_t val) {
    SIMAVR_OUTPUT_PORT1 = val;
}

inline_static_fn void debug_port2_out(uint8_t val) {
    SIMAVR_OUTPUT_PORT2 = val;
}

//
//debug_puts()
// Write the give string to the output port.
//
void debug_puts(const char *str) {
  const char *c;
  for(c = str; *c; c++) {
    debug_port2_out(*c);
  }
}

#else //#ifdef USING_SIMAVR
//////////////////////////////////////////////////////////////////////
// Not running in a simulator. Replace with dummy functions.
//////////////////////////////////////////////////////////////////////

inline_static_fn void debug_port0_out(uint8_t val) {}
inline_static_fn void debug_port1_out(uint8_t val) {}
inline_static_fn void debug_port2_out(uint8_t val) {}
inline_static_fn void debug_puts(const char *str){}

#endif

//////////////////////////////////////////////////////////////////////
// Simulator Independent Functions
//////////////////////////////////////////////////////////////////////

inline char debug_nibble_to_char(char val) {
#ifdef BUILD_DEBUGGING
    switch(val & 0xF) {
      case 0x0: return '0';
      case 0x1: return '1';
      case 0x2: return '2';
      case 0x3: return '3';
      case 0x4: return '4';
      case 0x5: return '5';
      case 0x6: return '6';
      case 0x7: return '7';
      case 0x8: return '8';
      case 0x9: return '9';
      case 0xA: return 'A';
      case 0xB: return 'B';
      case 0xC: return 'C';
      case 0xD: return 'D';
      case 0xE: return 'E';
      case 0xF: return 'F';
      default: break;
    }
    return ' ';
#endif
}

void debug_puti(int val) {
#ifdef BUILD_DEBUGGING
  int i;
  char out[] = "0x0000";

  for(i = 0; i < sizeof(int) / 4; ++i) {
    out[i + 2] = debug_nibble_to_char(val >> i);
  }

  debug_puts(out);
#endif
}

#endif //DEBUGGING_H
